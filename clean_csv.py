import os
import csv

count = 0

locations = []
job_title = []
av_sal = []
sal_range = []

as_array = []

csvpath =  os.path.join(os.path.dirname(os.path.abspath(__file__)), 'resources', 'salaries.csv')
with open(csvpath, newline='\n') as csvfile:

    # CSV reader specifies delimiter and variable that holds contents
    csvreader = csv.reader(csvfile, delimiter=',')

    #print(csvreader)

    # Read the header row first (skip this step if there is now header)
    #csv_header = next(csvreader)
    #print(f"CSV Header: {csv_header}")

    # Read each row of data after the header
    # get locations to array
    
    for row in csvreader:
        as_array.append(row) 

#print(as_array)

i = 0
while i < len(as_array):
    job_title.append(as_array[i])
    i+=4

j = 1
while j < len(as_array):
    locations.append(as_array[j])
    j+=4

k = 2
while k < len(as_array):
    av_sal.append(as_array[k])
    k+=4

l=3
while l < len(as_array):
    sal_range.append(as_array[l])
    l+=4

        

# write these arrays to new csv

cleaned_csv = zip(job_title, locations, av_sal, sal_range)

output_file = os.path.join('cleanedsal.csv')

#  Open the output file
with open(output_file, "w", newline="") as datafile:
    writer = csv.writer(datafile)

    # Write the header row
    writer.writerow(["title", "location", "av_sal", "sal_range"])

    # Write in zipped rows
    writer.writerows(cleaned_csv)

